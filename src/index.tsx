import { setupConfig } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(

  <IonReactRouter>
    <App />
  </IonReactRouter>,

  document.getElementById("root")
);

setupConfig({
  hardwareBackButton: false,
  swipeBackEnabled: false,
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
