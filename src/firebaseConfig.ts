import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAKnsFStc3zFA0hllMCSTgsxasuft_Hsr4",
    authDomain: "dhbw-informationsapp.firebaseapp.com",
    databaseURL: "https://dhbw-informationsapp.firebaseio.com",
    projectId: "dhbw-informationsapp",
    storageBucket: "dhbw-informationsapp.appspot.com",
    messagingSenderId: "44303337047",
    appId: "1:44303337047:web:d66927a8f75f1629c68512"
}

firebase.default.initializeApp(config)

export async function loginUser(email:string, password:string) {

    try {
    const res = await firebase.default.auth().signInWithEmailAndPassword(email, password)
    return true
    } catch(error) {
        return false
    }
}

export function getEmail() {
    return firebase.default.auth().currentUser?.email
}

export function isLoggedIn() {
    return firebase.default.auth().currentUser?.email
}

export function logoutUser() {
    firebase.default.auth().signOut()
}