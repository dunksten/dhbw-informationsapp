import { Plugins } from "@capacitor/core";

const { Storage } = Plugins;

class StorageHelper {
  async setItem(key:string, value:string):Promise<void> {
    await Storage.set({
      key: key,
      value: value,
    });
  }

  async getItem(key:string):Promise<string|null> {
    const {value} = await Storage.get({ key: key });
    return value;
  }
}

export default StorageHelper;
