import React from "react";
import { Route, Redirect, useHistory } from "react-router-dom";
import {
  IonApp,
  IonPage,
  IonRouterOutlet,
  IonSplitPane,
  useIonRouter,
} from "@ionic/react";

import MenuComponent from "./components/MenuComponent";

import Home from "./pages/Home";
import Login from "./pages/Login";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";

import { Plugins } from "@capacitor/core";

const App: React.FC = () => {
  let history = useHistory();
  const ionRouter = useIonRouter();

  Plugins.App.addListener("backButton", (e) => {
    if (!ionRouter.canGoBack()) {
      Plugins.App.exitApp();
    } else {
      history.goBack();
    }
  });

  return (
    <IonApp>
      <IonSplitPane contentId="main">
        <MenuComponent />

        <IonPage id="main">
          <IonRouterOutlet>
            <Route path="/home" component={Home}></Route>
            <Route path="/login" component={Login}></Route>
          </IonRouterOutlet>

          <Redirect from="/" to="/home" exact />
        </IonPage>
      </IonSplitPane>
    </IonApp>
  );
};

export default App;
