import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonContent,
  IonInput,
  IonItem,
  IonItemDivider,
  IonLabel,
  IonList,
  IonTitle,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import FooterComponent from "../components/FooterComponent";
import HeaderComponent from "../components/HeaderComponent";
import "./Login.css";
import { loginUser, getEmail, isLoggedIn, logoutUser } from "../firebaseConfig";

const Login: React.FC = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [loggedIn, setLogin] = useState<string>("false");
  const [user, setUser] = useState("string");

  async function login() {
    const res = await loginUser(email, password);
    if (res) {
      setLogin("true");
      const result = getEmail();
      setUser(result!!);
    }
  }

  useEffect(() => {
    if(isLoggedIn()){
      setLogin("true")
      setUser(getEmail()!!)
    }
  })

  function logout() {
    logoutUser();
    setLogin("false")
  }

  return (
    <>
      <HeaderComponent />
      <IonContent className="ion-padding">
        {loggedIn === "false" && (
          <>
            <IonTitle>Login</IonTitle>
            <IonList>
              <IonItem>
                <IonLabel>E-Mail</IonLabel>
                <IonInput
                  value={email}
                  onIonChange={(e: any) => setEmail(e.target.value)}
                ></IonInput>
              </IonItem>
              <IonItem>
                <IonLabel>Passwort</IonLabel>
                <IonInput
                  value={password}
                  onIonChange={(e: any) => setPassword(e.target.value)}
                ></IonInput>
              </IonItem>
              <IonButton onClick={login}>Login</IonButton>
            </IonList>
          </>
        )}
        {loggedIn === "true" &&
          <>
          <IonCard>
            <IonCardHeader>Eingeloggt</IonCardHeader>
            <IonCardContent>E-Mail: {user}</IonCardContent>
          </IonCard>
          <IonButton onClick={logout}>Logout</IonButton>
          </>
        }
      </IonContent>
      <FooterComponent />
    </>
  );
};

export default Login;
