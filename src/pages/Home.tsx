import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonContent,
  IonList,
  IonListHeader,
} from "@ionic/react";
import React, { useEffect, useState } from "react";

import "./Home.css";
import usePush from "../hooks/usePush";
import HeaderComponent from "../components/HeaderComponent";
import FooterComponent from "../components/FooterComponent";
import { isLoggedIn } from "../firebaseConfig";

const Home: React.FC = () => {
  
  const notifications = usePush();
  const [loggedIn, setLoggedIn] = useState<string>("false");

  useEffect(() => {
    if(isLoggedIn())setLoggedIn("true") 
  })

  return (
    <>
    <HeaderComponent />
    <IonContent>
      {loggedIn === "true" && 
        <IonList>
        <IonListHeader>Aktuelle Meldungen</IonListHeader>
        {notifications &&
          notifications.notifications.map((notif: any) => (
            <IonCard key={notif.id}>
              <IonCardHeader>
                <h3>{notif.title}</h3>
              </IonCardHeader>
              <IonCardContent>
                <p>{notif.body}</p>
              </IonCardContent>
            </IonCard>
          ))}
      </IonList>
      }
      {loggedIn === "false" && 
        <IonCard>
          <IonCardHeader>Nicht eingeloggt</IonCardHeader>
          <IonCardContent>Bitte einloggen</IonCardContent>
        </IonCard>
      }
      
    </IonContent>
    <FooterComponent />
    </>
  );
};

export default Home;
