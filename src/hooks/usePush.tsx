import { useState, useEffect } from "react";

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from "@capacitor/core";

const { PushNotifications } = Plugins;

const usePush = () => {
  const [notifications, setNotifications] = useState([{}]);

  useEffect(() => {
    console.log("State changed")
  }, [notifications]);

  PushNotifications.requestPermission().then(result => {
    if (result.granted) {
      // Register with Apple / Google to receive push via APNS/FCM
      PushNotifications.register();
    } else {
      alert("Push Notification Permission is required!")
    }
  });

  // On succcess, we should be able to receive notifications
  PushNotifications.addListener(
    "registration",
    (token: PushNotificationToken) => {
      console.log("Registration successful.");
      console.log(token.value);
    }
  );

  // Some issue with your setup and push will not work
  PushNotifications.addListener("registrationError", (error: any) => {
    console.log("Error on registration: " + JSON.stringify(error));
  });
  // Show us the notification payload if the app is open on our device
  PushNotifications.addListener(
    "pushNotificationReceived",
    (notification: PushNotification) => {
      let old = notifications;
      old.push({
        id: notification.id,
        title: notification.title,
        body: notification.body,
        data: notification.data,
      });
      setNotifications([...old]);
    }
  );

  // Method called when tapping on a notification
  PushNotifications.addListener(
    "pushNotificationActionPerformed",
    (notification: PushNotificationActionPerformed) => {
      alert(
        "Notification pressed: " +
          notification.notification.title +
          notification.notification.body
      );
    }
  );

  return { notifications };
};

export default usePush;
