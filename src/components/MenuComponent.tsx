import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonMenuToggle,
  IonThumbnail,
  IonImg,
} from "@ionic/react";

import React from "react";
import { useLocation } from "react-router-dom";
import {
  homeOutline,
  homeSharp,
  informationOutline,
  informationSharp,
  lockClosedOutline,
  lockClosedSharp,
} from "ionicons/icons";
import "./MenuComponent.css";

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const appPages: AppPage[] = [
  {
    title: "Aktuelle Meldungen",
    url: "/home",
    iosIcon: homeOutline,
    mdIcon: homeSharp,
  },
  {
    title: "Login",
    url: "/login",
    iosIcon: lockClosedOutline,
    mdIcon: lockClosedSharp,
  },
];

const MenuComponent: React.FC = () => {
  const location = useLocation();


  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList>
          <IonItem id="heading">
            <IonLabel>DHBW Informationsapp</IonLabel>
            <IonThumbnail>
              <IonImg src="assets/icon/icon.png" />
            </IonThumbnail>
          </IonItem>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem
                  className={
                    location.pathname === appPage.url ? "selected" : ""
                  }
                  routerLink={appPage.url}
                  routerDirection="none"
                  lines="none"
                  detail={false}
                >
                  <IonIcon
                    slot="start"
                    ios={appPage.iosIcon}
                    md={appPage.mdIcon}
                  />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default MenuComponent;
