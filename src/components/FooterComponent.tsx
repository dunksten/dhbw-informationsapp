import React from "react";
import "./FooterComponent.css";
import {
  IonToolbar,
  IonTitle,
  IonFooter,
} from "@ionic/react";

const FooterComponent: React.FC = () => {
  return (
    <IonFooter>
      <IonToolbar>
        <IonTitle>DHBW Informationsapp</IonTitle>
      </IonToolbar>
    </IonFooter>
  );
};

export default FooterComponent;
