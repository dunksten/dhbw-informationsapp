import React from "react";
import "./HeaderComponent.css";
import {
  IonToolbar,
  IonTitle,
  IonHeader,
  IonButtons,
  IonMenuButton,
  IonIcon,
  IonButton,
} from "@ionic/react";
import { useLocation } from "react-router";
import {
  ellipsisHorizontalOutline,
  ellipsisHorizontalSharp,
} from "ionicons/icons";

function capitalize(str: string): string {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

const HeaderComponent: React.FC = () => {
  const location: string = capitalize(useLocation().pathname.slice(9));
  return (
    <IonHeader>
      <IonToolbar>
        <IonButtons slot="start">
          <IonMenuButton />
        </IonButtons>
        <IonTitle>{location}</IonTitle>
        <IonButtons slot="primary">
          <IonButton>
            <IonIcon
              slot="icon-only"
              ios={ellipsisHorizontalOutline}
              md={ellipsisHorizontalSharp}
            />
          </IonButton>
        </IonButtons>
      </IonToolbar>
    </IonHeader>
  );
};

export default HeaderComponent;
