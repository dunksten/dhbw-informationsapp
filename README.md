# Studienarbeit: DHBW Informationsapp
## Von: Jan Hansel
## Betreuer: Prof. Dr. Kai Becher

<br />
<br />
<br />

# Requirements:
- Ionic must be installed. To do so, read: [Ionic Cli Installation](https://ionicframework.com/docs/intro/cli).
- Open a command line window in the root folder of the app and type ``` ionic serve ```.
- Your browser should now display the app.